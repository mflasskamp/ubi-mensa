#!/usr/bin/python3
'''
update on vps:
    scp .\mensa_tg.py ionos:/root/ubi-mensa/
'''
import asyncio
import platform
import mensa_api
from telegram.ext import Application

from os import getenv
from dotenv import load_dotenv
load_dotenv()

bot_token = getenv('UBIMENSA_BOT_TOKEN')
chat_id = getenv('UBIMENSA_CHAT_ID')

app = Application.builder().token(bot_token).build()

if platform.system() == "Windows":
    asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())


async def tg_send(chat_id, msg, msg_id=None):
    tgmsg = await app.bot.send_message(chat_id=chat_id, text=msg, parse_mode="HTML", disable_notification=True)
    return tgmsg['message_id']


def main():
    msg_d = mensa_api.fetch_menu_dict()
    msg = mensa_api.get_menu_html(msg_d)
    if not msg:
        exit(-1)

    loop = asyncio.get_event_loop()
    msg_id = loop.run_until_complete(tg_send(chat_id, msg))

    mensa_api.write_menu_csv(msg_d, msg_id)

if __name__ == "__main__":
    main()
